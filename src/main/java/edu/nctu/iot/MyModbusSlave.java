package edu.nctu.iot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyModbusSlave {
	static final Logger LOG = LoggerFactory.getLogger(MyModbusSlave.class);

	public MyModbusSlave() {		
	}
	
	public static void main(String[] args) throws Exception {
		AZ8778 az8778 = new AZ8778();
		az8778.start();		
	}
}
