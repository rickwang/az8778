package edu.nctu.iot;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cht.iot.modbus.ReversedModbusConverter;
import com.cht.iot.modbus.Slaves;
import com.cht.iot.modbus.Type;

import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

public class AZ8778 {
	static final Logger LOG = LoggerFactory.getLogger(AZ8778.class);
	
	String host = "localhost";
	int port = 10500;
	String id = "AZ8778";
	
	String tty = "/dev/ttyUSB0";
	int timeout = 2000;
	int baudrate = 9600;
	int databits = 8;
	int stopbits = 1;
	int parity = SerialPort.PARITY_NONE;
	
	Slaves slaves = new Slaves();
	SerialPort serial;
	
	public AZ8778() {
	}
	
	public void start() throws Exception {
		ReversedModbusConverter p = new ReversedModbusConverter(slaves);
		p.setHost(host);
		p.setPort(port);
		p.setConnectionId(id);		
		p.start();
		
		LOG.info("Opening {}", tty);
		
		serial = (SerialPort) CommPortIdentifier.getPortIdentifier(tty).open(tty, timeout);
		serial.setSerialPortParams(baudrate, databits, stopbits, parity);
		
		LOG.info("Wait for message from {}", tty);
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				process();				
			}
			
		}).start();
	}
	
	void process() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(serial.getInputStream()));
			String ln;		
			while ((ln = br.readLine()) != null) {
				LOG.info("RECV - {}", ln);
				
				if (ln.startsWith("W")) {
					try {
						StringTokenizer st = new StringTokenizer(ln, "WC:TH%");
						
						for (int i = 0;i < 4;i++) {					
							int value = (int) (Double.parseDouble(st.nextToken()) * 10);						
							slaves.update(1, Type.holding_register, i, value);
						}
								
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Error", e);
		}
	}
}
