package com.cht.iot.modbus;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModbusProtocol {
	protected final static Logger LOG = LoggerFactory.getLogger(ModbusProtocol.class);
	
	protected final static int MAX_MODBUS_PDU_SIZE = 265;
	
	final static int EC_ERROR = 0x80;
	final static int EC_ILLEGAL_FUNCTION = 0x01;
	
	final static int FC_READ_COILS = 0x01;
	final static int FC_READ_DISCRETE_INPUTS = 0x02;
	final static int FC_WRITE_SINGLE_COIL = 0x05;
	final static int FC_WRITE_MULTIPLE_COILS = 0x0F;
	
	final static int FC_READ_INPUT_REGISTERS = 0x04;
	final static int FC_READ_HOLDING_REGISTERS = 0x03;
	final static int FC_WRITE_SINGLE_REGISTER = 0x06;
	final static int FC_WRITE_MULTIPLE_REGISTERS = 0x10;
	final static int FC_REGISTER_ERROR = 0x0F0;
		
	public ModbusProtocol() {	
	}

	protected void read(InputStream is, byte[] bytes) throws IOException {
		int i = 0;
		int s;
		
		while ((i < bytes.length) && (s = is.read(bytes, i, bytes.length - i)) > 0) {
			i += s;
		}
		
		if (i < bytes.length) {
			throw new EOFException();
		}
	}
	
	protected int read(InputStream is) throws IOException {
		int c = is.read();
		if (c < 0) {
			throw new EOFException();
		}		
		return c;
	}
	
	protected int readShort(InputStream is) throws IOException {
		return (read(is) << 8) | read(is);
	}
	
	protected int readInt(InputStream is) throws IOException {
		return (read(is) << 24) | (read(is) << 16) | (read(is) << 8) | read(is);
	}
	
	protected void write(OutputStream os, int b) throws IOException {
		os.write(b);		
	}
	
	protected void write(OutputStream os, byte[] b) throws IOException {
		os.write(b);		
	}
	
	protected void writeShort(OutputStream os, int v) throws IOException {
		os.write((v & 0x0FF00) >> 8);
		os.write(v & 0x0FF);
	}
	
	protected byte[] toBytes(int[] values) {
		int s = values.length / 8;
		int a = values.length % 8;		
		if (a != 0) {
			s += 1;
		}
		
		byte[] bytes = new byte[s];
		int x = -1;
		
		for (int i = 0;i < values.length;i++) {
			int d = i % 8;			
			if (d == 0) {
				x += 1;
			}
			
			if (values[i] != 0) {			
				bytes[x] |= (1 << d);
			}
		}	
		
		return bytes;
	}
	
	protected int[] toValues(byte[] bytes, int quantity) {
		int[] values = new int[quantity]; 
		
		int x = -1;
		for (int i = 0;i < values.length;i++) {
			int d = i % 8;			
			if (d == 0) {
				x += 1;
			}
			
			values[i] = ((bytes[x] & (1 << d)) != 0)? 1 : 0;						
		}
		
		return values;
	}
	
	@SuppressWarnings("unused")
	public void service(int slaveId, Slaves slaves, InputStream dis, OutputStream dos) throws IOException {
		int fc = read(dis);
		
		if (LOG.isDebugEnabled()) LOG.debug("Function Code: " + fc);

		if ((fc == FC_READ_INPUT_REGISTERS) || (fc == FC_READ_HOLDING_REGISTERS)) {
			int address = readShort(dis);
			int quantity = readShort(dis);
			
			if (LOG.isDebugEnabled()) LOG.debug(String.format("Read Registers - address: %d, quantity: %d", address, quantity));
			
			write(dos, fc); // FC_READ_INPUT_REGISTERS or FC_READ_HOLDING_REGISTERS
			write(dos, quantity * 2); // byte count
			
			Type type = (fc == FC_READ_INPUT_REGISTERS)? Type.input_register : Type.holding_register;
			
			int[] values = slaves.get(slaveId, type, address, quantity);
			for (int value : values) {
				writeShort(dos, value);
			}
		} else if (fc == FC_WRITE_SINGLE_REGISTER) {
			int address = readShort(dis);
			int value = readShort(dis);
			
			if (LOG.isDebugEnabled()) LOG.debug(String.format("Write Single Register - address: %d, value: %d", address, value));
			
			slaves.update(slaveId, Type.holding_register, address, value);
			
			write(dos, fc);
			writeShort(dos, address);
			writeShort(dos, value);
			
		} else if (fc == FC_WRITE_MULTIPLE_REGISTERS) {
			int address = readShort(dis);
			int quantity = readShort(dis);
			int bc = read(dis); // byte count
			
			if (LOG.isDebugEnabled()) LOG.debug(String.format("Write Multiple Registers - address: %d, quantity: %d", address, quantity));
			
			for (int i = 0;i < quantity;i++) {
				int value = readShort(dis);
				slaves.update(slaveId, Type.holding_register, address + i, value);
			}
			
			write(dos, fc);
			writeShort(dos, address);
			writeShort(dos, quantity);
			
		} else if ((fc == FC_READ_COILS) || (fc == FC_READ_DISCRETE_INPUTS)) {
			int address = readShort(dis);
			int quantity = readShort(dis);
			
			if (LOG.isDebugEnabled()) LOG.debug(String.format("Read Coils - address: %d, quantity: %d", address, quantity));

			Type type = (fc == FC_READ_COILS)? Type.coil : Type.discrete_input;
			int[] values = slaves.get(slaveId, type, address, quantity);
			byte[] bytes = toBytes(values);
			
			write(dos, fc);
			write(dos, bytes.length);
			write(dos, bytes);
			
		} else if (fc == FC_WRITE_SINGLE_COIL) {
			int address = readShort(dis);
			int value = readShort(dis);
			
			if (LOG.isDebugEnabled()) LOG.debug(String.format("Write Single Coil - address: %d, value: %d", address, value));
			
			slaves.update(slaveId, Type.coil, address, (value == 0x0000)? 0 : 1); // 0x0000 or 0xFF00
			
			write(dos, fc);
			writeShort(dos, address);
			writeShort(dos, value);
			
		} else if (fc == FC_WRITE_MULTIPLE_COILS) {
			int address = readShort(dis);
			int quantity = readShort(dis);
			int bc = read(dis); // byte count
			byte[] bytes = new byte[bc];
			read(dis, bytes);
			
			if (LOG.isDebugEnabled()) LOG.debug(String.format("Write Multiple Coils - address: %d, quantity: %d", address, quantity));
			
			int[] values = toValues(bytes, quantity);
			
			for (int i = 0;i < values.length;i++) {
				slaves.update(slaveId, Type.coil, address + i, values[i]);
			}
			
			write(dos, fc);
			writeShort(dos, address);
			writeShort(dos, quantity);
			
		} else if (fc == FC_REGISTER_ERROR) {
			int code = readInt(dis);
			throw new IOException(String.format("Register error - fc: %02X, code: %d", fc, code));
			
		} else {
			write(dos, EC_ERROR + fc);
			write(dos, EC_ILLEGAL_FUNCTION);
		}
	}
}
