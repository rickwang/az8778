package com.cht.iot.modbus;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ReversedModbusConverter {
	static final Logger LOG = LoggerFactory.getLogger(ReversedModbusConverter.class);

	Slaves slaves;
	
	String host = "localhost";
	int port = 10500;	
	int timeout = 2000;
	int waiting = 300000;
	String connectionId = "000000";
		
	public ReversedModbusConverter(Slaves slaves) {
		this.slaves = slaves;
	}
	
	public void setHost(String host) {
		this.host = host;
	}
	
	public void setPort(int port) {
		this.port = port;
	}
	
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	
	public void setWaiting(int waiting) {
		this.waiting = waiting;
	}
	
	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}
	
	public void start() {
		Thread thr = new Thread(new Runnable() {
			@Override
			public void run() {
				ReversedModbusConverter.this.run();				
			}
		});
		
		thr.start();
	}
	
	// ======
	
	protected Socket newSocket() throws Exception {
//		if ("TLS".equalsIgnoreCase(this.type)) {
//			SSLContext ssl = EasySSLContextFactory.createClientSSLContext();
//			return ssl.getSocketFactory().createSocket();
//			
//		} else {
			return new Socket();
//		}		
	}
	
	// ======
	
	protected void run() {
		for (;;) {
			Socket sck = null;
			try {
				sck = this.newSocket();
				
				LOG.info(String.format("Connect to %s:%d", this.host, this.port));
				
				sck.connect(new InetSocketAddress(this.host, this.port), this.timeout);
				sck.setSoTimeout(this.waiting);
				
				OutputStream os = sck.getOutputStream();
				InputStream is = sck.getInputStream();
				
				os.write(String.format("+register:%s;", this.connectionId).getBytes());
				os.flush();
		
				ModbusRtuProtocol mtp = new ModbusRtuProtocol(this.slaves);
				
				DataInputStream dis = new DataInputStream(is);
				DataOutputStream dos = new DataOutputStream(os);
				
				for (;;) {
					mtp.service(dis, dos);
					dos.flush();
				}
			} catch (Exception e) { 
				LOG.error(e.getMessage(), e);
				
			} finally {
				if (sck != null) {
					try { sck.close(); } catch (Exception ex) {}
				}
			}
			
			try {
				LOG.info("Delay {} ms and retry.", timeout);
				Thread.sleep(timeout);
				
			} catch (Exception e) {				
			}
		}
	}
}
