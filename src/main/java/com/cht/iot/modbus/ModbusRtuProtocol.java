package com.cht.iot.modbus;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ModbusRtuProtocol extends ModbusProtocol {
	final Slaves slaves;

	public ModbusRtuProtocol(Slaves slaves) {
		this.slaves = slaves;
	}
	
	public void service(InputStream dis, OutputStream dos) throws IOException {
		CatchedInputStream cis = new CatchedInputStream(dis);
				
		int ui = read(cis); // unit identifier
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream(MAX_MODBUS_PDU_SIZE); // function code (1) + address (2) + quantity (2) + data (125 * 2)
		
		write(baos, ui); // response unit identifier first
		
		service(ui, slaves, cis, baos); // Pure Modbus Protocol
				
		int crchi = read(cis); // CRC High
		int crclo = read(cis); // CRC Low
		
		byte[] rs = cis.take();		
		if (LOG.isDebugEnabled()) {
			LOG.debug("RX - " + Utils.toHexString(rs));
		}

		int[] crc = Utils.crc(rs, 0, rs.length - 2);
		if ((crc[0] != crchi) || (crc[1] != crclo)) {
			throw new IOException(String.format("CRC error! Expect [%02X] [%02X], but [%02X] [%02X]", crc[0], crc[1], crchi, crclo));
		}
		
		baos.flush();		
		byte[] ds = baos.toByteArray(); // function code + data
		
		crc = Utils.crc(ds);
		
		write(baos, crc[0]);
		write(baos, crc[1]);
		
		baos.flush();
		
		ds = baos.toByteArray();
		
		if (LOG.isDebugEnabled()) {
			LOG.debug("TX - " + Utils.toHexString(ds));
		}

		write(dos, ds);
	}	
}
