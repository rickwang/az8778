package com.cht.iot.modbus;

import java.io.ByteArrayOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CatchedInputStream extends FilterInputStream {
	ByteArrayOutputStream buffer = new ByteArrayOutputStream(512);
	
	public CatchedInputStream(InputStream in) {
		super (in);
	}
	
	@Override
	public int read() throws IOException {
		int b = super.read();
		
		if (b >= 0) {
			buffer.write(b);
		}
		
		return b;
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		int s = super.read(b, off, len);
		
		buffer.write(b, off, len);
		
		return s;
	}
	
	public byte[] take() throws IOException {
		byte[] b = buffer.toByteArray();
		buffer.reset();
		
		return b;
	}
}
