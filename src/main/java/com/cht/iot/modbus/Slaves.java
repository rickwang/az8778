package com.cht.iot.modbus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Slaves {
	final List<Registers> registers = new ArrayList<Registers>();
	
	final List<RegisterChangedListener> listeners = Collections.synchronizedList(new ArrayList<RegisterChangedListener>());

	public Slaves() {		
	}

    public void destroy() {
    }

	public void addRegisterChangedListener(RegisterChangedListener listener) {
		listeners.add(listener);
	}

	protected Registers getRegisters(int slaveId) {
		for (Registers r : registers) {
			if (r.getSlaveId() == slaveId) {
				return r;
			}
		}
		
		Registers r = new Registers(slaveId); // create new one
		registers.add(r);
		
		return r;		
	}
	
	public synchronized int get(int slaveId, Type type, int address) {
		Registers register = getRegisters(slaveId);
		return register.get(type, address);
	}
	
	public synchronized int[] get(int slaveId, Type type, int address, int quantity) {
		Registers register = getRegisters(slaveId);
		return register.get(type, address, quantity);
	}
	
	public synchronized void update(int slaveId, Type type, int address, int[] value) {
		for (int i = 0;i < value.length;i++) {
			update(slaveId, type, address + i, value[i]);
		}
	}
	
	public synchronized void update(int slaveId, Type type, int address, int value) {
		Registers register = getRegisters(slaveId);
		register.update(type, address, value);

        for (RegisterChangedListener listener : listeners) {
            listener.onRegisterChanged(slaveId, type, address, value);
        }
	}
}
