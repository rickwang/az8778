package com.cht.iot.modbus;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

public class Registers implements Serializable {
	private static final long serialVersionUID = 1L;
		
	final int slaveId;

	final Space[] spaces = new Space[] {
			new Space(Type.coil),
			new Space(Type.discrete_input),
			new Space(Type.input_register),
			new Space(Type.holding_register)
	};
	
	public Registers(int slaveId) {
		this.slaveId = slaveId;
	}
	
	public int getSlaveId() {
		return slaveId;
	}
	
	protected Space getSpace(Type type) {
		for (Space space : spaces) {
			if (space.type == type) {
				return space;
			}
		}
		
		throw new NoSuchElementException(String.valueOf(type));
	}

	public void update(Type type, int address, int value) {
		Space space = getSpace(type);
		space.values.put(address, value);
	}
	
	public int get(Type type, int address) {		
		Space space = getSpace(type);
		Integer value = space.values.get(address);
		return (value != null)? value : 0;
	}
	
	public int[] get(Type type, int address, int quantity) {
		int[] values = new int[quantity];
		for (int i = 0;i < quantity;i++) {
			values[i] = get(type, address + i);
		}
		
		return values;
	}
	
	static class Space implements Serializable {
		private static final long serialVersionUID = 1L;
		
		final Type type;
		final Map<Integer, Integer> values = new HashMap<Integer, Integer>(); // address -> value
		
		public Space(Type type) {
			this.type = type;
		}
	}
}
