package com.cht.iot.modbus;

public interface RegisterChangedListener {

	void onRegisterChanged(int slaveId, Type type, int address, int value);
}
