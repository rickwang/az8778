package com.cht.iot.modbus;

public enum Type {
    coil, discrete_input, input_register, holding_register
}
